# GliotoxinHealth
aaron gowins  
June 1, 2015  


## Wordclouds and Analysis 

We have a number of libraries to load, so make sure to install.packages() if these don't look familiar. If you want to see any of the manipulations that follow call inspect(docs[1]) before and after to see the difference. Most are at least vaguely self-explanatory.

Much of this is pulled from http://onepager.togaware.com/TextMiningO.pdf, which has many other terrific posts, and I highly recommend for clarity and for being thorough but brief.

http://amunategui.github.io/pubmed-query/ also has some great info, and is a quick read.


```r
library(SnowballC)
library(tm)
library(qdap)
library(ggplot2)
library(scales)
library(dplyr)
library(wordcloud)
```

## PubMed Gliotoxin Search

Gliotoxin in a powerful poison produced by many strains of infectious fungi. It works by inhibiting the binding of the transcription factor NFkB, which is a key factor in human immune response.

We would like to collect all articles in PubMed that refer to gliotoxin, which, according to the obscurity of our keyword and our recollection that it was discovered in 1993, we can assume will be a fairly small collection.

RISmed is by far the most complete package for text mining in R, but there are some really cool functions in others like pubmed.mineR. These are extremely small-scale examples of what text mining can be, and real queries can take enormous space, so be ready if you want to search "cancer" to add some severe restrictions.

We will examine the titles and abstracts.



```r
library(RISmed)
```

```
## Warning: package 'RISmed' was built under R version 3.1.3
```

```r
library(plyr)
```

```
## Warning: package 'plyr' was built under R version 3.1.3
```

```r
p<-"gliotoxin"
res <- EUtilsSummary(p, type="esearch", db="pubmed")
date()
```

```
## [1] "Tue Aug 11 16:59:27 2015"
```

```r
fetch <- EUtilsGet(res,type="efetch", db="pubmed")
```

```
## Warning in Medline(Result, query): NAs introduced by coercion
```

```
## Warning in Medline(Result, query): NAs introduced by coercion
```

```r
count<-table(YearReceived(fetch))
count<-as.data.frame(count)
articles<-data.frame('Title'=ArticleTitle(fetch),'Abstract'=AbstractText(fetch),'Year'=YearReceived(fetch))
#head(articles,1)
abstracts<-as.character(articles$Abstract)
#head(abstracts)
#set.seed(223)
#articlesX<-split(articles,as.factor("Year"))
#head(articles$Year,1)
#dim(articles)
wordcloud(abstracts,min.freq=10,max.words=70, colors=brewer.pal(7,"Set1"))
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-1.png) 

```r
articlesC<-aggregate(x=articles,by=list(articles$Year),FUN = function(X) paste(unique(X), collapse=", "))





  simplefunc <- function(articlesC) {
          print(articlesC$Year)
       articlesC$Abstract<-as.character(articlesC$Abstract)
       freqList<-table(articlesC$Abstract)
freqList<-sort(freqList,decreasing=TRUE)
head(freqList)
 wordcloud(articlesC$Abstract,min.freq=1,max.words=30, colors=brewer.pal(7,"Set1"))
}



articles$Abstract<-as.character(articles$Abstract)
articles$Title<-as.character(articles$Abstract)
d_ply(articlesC,.(Year),  .fun=simplefunc) 
```

```
## [1] "1993"
```

```
## Warning in wordcloud(articlesC$Abstract, min.freq = 1, max.words = 30,
## colors = brewer.pal(7, : bioassay could not be fit on page. It will not be
## plotted.
```

```
## Warning in wordcloud(articlesC$Abstract, min.freq = 1, max.words = 30,
## colors = brewer.pal(7, : mycotoxins could not be fit on page. It will not
## be plotted.
```

```
## Warning in wordcloud(articlesC$Abstract, min.freq = 1, max.words = 30,
## colors = brewer.pal(7, : gliotoxin could not be fit on page. It will not be
## plotted.
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-2.png) 

```
## [1] "1995"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-3.png) 

```
## [1] "2000"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-4.png) 

```
## [1] "2002"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-5.png) 

```
## [1] "2003"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-6.png) 

```
## [1] "2004"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-7.png) 

```
## [1] "2005"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-8.png) 

```
## [1] "2006"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-9.png) 

```
## [1] "2007"
```

```
## Warning in wordcloud(articlesC$Abstract, min.freq = 1, max.words = 30,
## colors = brewer.pal(7, : glutamate could not be fit on page. It will not be
## plotted.
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-10.png) 

```
## [1] "2008"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-11.png) 

```
## [1] "2009"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-12.png) 

```
## [1] "2010"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-13.png) 

```
## [1] "2011"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-14.png) 

```
## [1] "2012"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-15.png) 

```
## [1] "2013"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-16.png) 

```
## [1] "2014"
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-17.png) 

```
## [1] "2015"
```

```
## Warning in wordcloud(articlesC$Abstract, min.freq = 1, max.words = 30,
## colors = brewer.pal(7, : compounds could not be fit on page. It will not be
## plotted.
```

```
## Warning in wordcloud(articlesC$Abstract, min.freq = 1, max.words = 30,
## colors = brewer.pal(7, : integrity could not be fit on page. It will not be
## plotted.
```

```
## Warning in wordcloud(articlesC$Abstract, min.freq = 1, max.words = 30,
## colors = brewer.pal(7, : compound could not be fit on page. It will not be
## plotted.
```

```
## Warning in wordcloud(articlesC$Abstract, min.freq = 1, max.words = 30,
## colors = brewer.pal(7, : levels could not be fit on page. It will not be
## plotted.
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-18.png) 

```r
        abstractsY<-articles[which(articles$Year==2015),]
dim(abstractsY)
```

```
## [1] 3 3
```

```r
       abstractsY<-as.character(abstractsY$Abstract)
#abstractsY
set.seed(223)
wordcloud(abstractsY,min.freq=1,max.words=30, colors=brewer.pal(7,"Set1"))
```

```
## Warning in wordcloud(abstractsY, min.freq = 1, max.words = 30, colors
## = brewer.pal(7, : expression could not be fit on page. It will not be
## plotted.
```

```
## Warning in wordcloud(abstractsY, min.freq = 1, max.words = 30, colors
## = brewer.pal(7, : development could not be fit on page. It will not be
## plotted.
```

```
## Warning in wordcloud(abstractsY, min.freq = 1, max.words = 30, colors =
## brewer.pal(7, : compounds could not be fit on page. It will not be plotted.
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-19.png) 

```r
head(count,1)
```

```
##   Var1 Freq
## 1 1993    1
```

```r
write.table(articles,"/Users/gowinsja/Desktop/txt_files/AF.txt")
names(count)<-c("Year","Counts")
ccount <- data.frame(Year=count$Year, Counts=cumsum(count$Counts)) 
ccount$g <- "g"
names(ccount) <- c("Year","Counts","g")
head(ccount)
```

```
##   Year Counts g
## 1 1993      1 g
## 2 1995      2 g
## 3 2000      5 g
## 4 2002      6 g
## 5 2003     10 g
## 6 2004     15 g
```

```r
q <- qplot(x=Year, y=Counts, data=count, geom="bar", stat="identity")
q <- q + geom_line(aes(x=Year, y=Counts, group=g), data=ccount) +
    ggtitle(paste("PubMed articles containing \'",p,"\'", sep="")) +
    ylab("Number of articles") +
    xlab(paste("Year \n Query time: ",Sys.time(), sep="")) +
    labs(colour="") +
    theme_bw()
q + theme(legend.position=c(0.2,0.85)) +
    annotate("text", x=max(as.numeric(ccount$Year)), y=max(ccount$Counts), label=max(ccount$Counts))
```

![](Gliotoxin_files/figure-html/unnamed-chunk-2-20.png) 





